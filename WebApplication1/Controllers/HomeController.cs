﻿using System.Web.Mvc;
using System.Data.Entity;
using WebApplication1.Models;
using WebApplication1.ViewModel;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {

        UsuarioModel usuario = new UsuarioModel();
        public ActionResult Index()
        {
            var listaUsuario = usuario.Listar();
            return View(listaUsuario);
        }

        [HttpPost]
        public JsonResult EditUsuario(int UsuarioId)
        {
            var _usuario = usuario.UsuareioId(UsuarioId);

            return Json(_usuario);
        }

        [HttpGet]
        public ActionResult ModalUsuario()
        {
            UsuarioViewModel usuario = new UsuarioViewModel();
            return PartialView("_CreateUsuario", usuario);
        }

        public JsonResult DeleteUsuario(int UsuarioId)
        {
            bool delete = usuario.Deletar(UsuarioId);
            string result = string.Empty;

            if (delete)
            {
                result = "Usuario deletado com sucesso.";
            }
            else
            {
                result = "Usuario não deletado.";
            }
            return Json(result);
        }

        public JsonResult SaveUsuario(UsuarioViewModel usuarioModel)
        {
            bool save = usuario.Salvar(usuarioModel);
            string result = string.Empty;

            if (save)
            {
                result = "Usuario salvo com sucesso.";
            }
            else
            {
                result = "Erro ao salvar usuario.";
            }
            return Json(result);
        }
    }
}