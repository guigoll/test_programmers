﻿
function Save() {
    $.ajax({
        url: urlSave,
        type: 'POST',
        data: $('form').serialize(),
        success: function (resp) {
            window.location.reload()
            $("#msg").append(resp.result)
        },
        error: function (err) { alert("Error: " + err.responseText); }
    })
};

function Remove(UsuarioId) {
    $.ajax({
        url: urlDelete,
        data: { UsuarioId: UsuarioId },
        type: 'POST',
        success: function () {
            window.location.reload()
            $("#msg").append(resp.result)            
        },
        error: function (err) { alert("Error: " + err.responseText); }
    });
};

function Edit(UsuarioId) {
    ModalUsuario();
    $.ajax({
        url: urlEdit,
        type: 'POST',
        data: { UsuarioId: UsuarioId },
        success: function (res) {
            Cancel();
            $("#IsUpdate").val(true);
            $("#Id").val(res.UsuarioId); 
            $("#Nome").val(res.Nome); 
            $("#Email").val(res.Email); 
            $("#Telefone").val(res.Telefone); 
            
        },
        error: function (err) { alert("Error: " + err.responseText); }
    })
};

function ModalUsuario(){
    $.ajax(
        {
            type: 'GET',
            url: urlGet,
            dataType: 'html',
            cache: false,
            async: true,
            success: function (data) {
                $('#Modal').html(data);
                $('#cadastro-usuario').modal('show');
            }
        });
}


function Cancel() {
    $("#IsUpdate").val('');
    $("#Id").val('');
    $("#Nome").val('');
    $("#Email").val('');
    $("#Telefone").val('')
}
