﻿using AutoMapper;
using WebApplication1.Models;
using WebApplication1.ViewModel;

namespace WebApplication1.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Usuario, UsuarioViewModel>();
        }
    }
}