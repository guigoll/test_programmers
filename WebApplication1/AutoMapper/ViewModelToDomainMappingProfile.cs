﻿using AutoMapper;
using WebApplication1.Models;
using WebApplication1.ViewModel;

namespace WebApplication1.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        protected override void Configure()
        {
            #region U
            CreateMap<UsuarioViewModel, Usuario>();
            #endregion
        }
    }
}