﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication1.ViewModel
{
    public class UsuarioViewModel
    {
        [Key]
        public int UsuarioId { get; set; }
        [StringLength(50, ErrorMessage = "O campo Nome permite no máximo 50 caracteres!")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Informe o Email")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Email inválido.")]
        public string Email { get; set; }
        public string Telefone { get; set; }
        public bool IsUpdate { get; set; }
    }
}