﻿using AutoMapper;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebApplication1.ViewModel;

namespace WebApplication1.Models
{
    public class UsuarioModel
    {
        private ProgrammersContext db = new ProgrammersContext();
        public bool Salvar(UsuarioViewModel usuarioViewModel)
        {
            var usuario = Mapper.Map<UsuarioViewModel, Usuario>(usuarioViewModel);

            if (usuarioViewModel.IsUpdate)
            {
                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();

                return true;
            }
            else
            {
                db.Usuario.Add(usuario);
                db.SaveChanges();
                return true;
            }
        }

        public bool Deletar(int UsuarioId)
        {
            if (UsuarioId == 0)
            {
                return false;
            }
            else
            {
                var usuario = db.Usuario.Find(UsuarioId);
                db.Usuario.Remove(usuario);
                db.SaveChanges();
                return true;             
            }       
        }

        public List<UsuarioViewModel> Listar()
        {
           var listaUsuario = db.Usuario.ToList();

            if (listaUsuario.Count > 0)
            {
                return Mapper.Map<List<Usuario>, List<UsuarioViewModel>>(listaUsuario);               
            }
            else
            {
                return null;
            }
        }

        public UsuarioViewModel UsuareioId(int UsuarioId)
        {
            var usuario = db.Usuario.Find(UsuarioId);

            return Mapper.Map<Usuario, UsuarioViewModel>(usuario);
        }      
    }
}