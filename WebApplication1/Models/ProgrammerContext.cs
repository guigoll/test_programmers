﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace WebApplication1.Models
{
    public class ProgrammersContext : DbContext
    {    
        public ProgrammersContext()
            :base("name=ProgrammerConnectionString")
        {
        }

        public DbSet<Usuario> Usuario { get; set; }

    }
}