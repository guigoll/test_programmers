﻿using System.Data.Entity;

namespace WebApplication1.Models
{
    public class ProgrammersContext : DbContext
    {
        public ProgrammersContext()
            : base("name=conexao")
        {
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            base.OnModelCreating(modelBuilder);
            
        }

        public DbSet<Usuario> Usuario { get; set; }

    }
}